var GrovePi = require("./sensors");
var dataHandler = require("./dataHandler");
var currentDHTValue;
var currentBarometerValue;

exports = module.exports = function(io) {
    //Evénènement déclenché à chaque nouvelle connection d'un client
    io.sockets.on("connection", function(socket) {

        socket.emit("GetDHTValue", currentDHTValue);
        socket.emit("GetDHTValue", currentDHTValue);
        //Evenement éxécuté lors d'un clique de l'utilisateur sur le bouton "Générer le graphe"
        socket.on("requestData", function(startDate, endDate, value) {
            dataHandler.RequestData(startDate, endDate, value);
            socket.emit("returnData", data);
        });

        GrovePi.dhtEvent.on("dhtSensorChange", function(res) {
            socket.emit("GetDHTValue", currentDHTValue);
        });

        GrovePi.barometerEvent.on("barometerSensorChange", function(res) {
            socket.emit("GetBarometerValue", currentBarometerValue);
        });
    });
}
setInterval(function() {
    if (currentDHTValue && currentBarometerValue) {
        dataHandler.SaveData(currentDHTValue, currentBarometerValue);
    }
}, 60000);

GrovePi.dhtEvent.on("dhtSensorChange", function(res) {
    currentDHTValue = res;
});

GrovePi.barometerEvent.on("barometerSensorChange", function(res) {
    currentBarometerValue = res;
});