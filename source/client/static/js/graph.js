function include(list, item) {
    return (list.indexOf(item) != -1);
}
//Fonction qui dessine le graphe
function drawChart(dataList) {
    for (var i = 0; i < dataList.length; i++) {
        dataList[i][0] = new Date(dataList[i][0]);
    }
    color = [];
    column2 = "";
    title1 = "";
    title2 = "";
    mainTitle = "";

    if (include(values, "Température") && include(values, "Humidité")) {
        title1 = "Température (C)";
        title2 = "Humidité relative (%)";
        color.push('#ff0000');
        color.push('#006bff');
        mainTitle = "Evolution de la tempértaure et de l'humidité relative"
    } else if (include(values, "Température") && include(values, "Pression")) {
        title1 = "Température (C)";
        title2 = "Pression atmosphérique (hPa)";
        color.push('#ff0000');
        color.push('#b89502');
        mainTitle = "Evolution de la tempértaure et de la pression atmosphérique"
    } else if (include(values, "Humidité") && include(values, "Pression")) {
        title1 = "Humidité relative (%)";
        title2 = "Pression atmosphérique (hPa)";
        color.push('#006bff');
        color.push('#b89502');
        mainTitle = "Evolution de l'humidité relative et de la pression atmosphérique"
    } else if (include(values, "Humidité")) {
        title1 = "Humidité relative (%)";
        color.push('#006bff');
        mainTitle = "Evolution de l'humidité relative";
    } else if (include(values, "Température")) {
        title1 = "Température (C)";
        color.push('#ff0000');
        mainTitle = "Evolution de la tempértaure";
    } else {
        title1 = "Pression atmosphérique (hPa)";
        color.push('#b89502');
        mainTitle = "Evolution de la pression atmosphérique";
    }

    var dataTable = new google.visualization.DataTable();

    options.colors = color;
    options.title = mainTitle;

    dataTable.addColumn('date', 'Temps');
    if (title2) {
        options.vAxes = {
            0: {
                title: title1
            }, //Grâce à l'indexisation, on peut nommer les axes Y
            1: {
                title: title2
            }
        };
        dataTable.addColumn('number', title1);
        dataTable.addColumn('number', title2);
    } else {
        options.vAxes = {
            0: {
                title: title1
            }
        };
        dataTable.addColumn('number', title1);
    }
    //On ajoute nos données
    dataTable.addRows(dataList);
    //Div dans laquelle on veut insérer le graphe
    var chartDiv = document.getElementById('chart');
    //On insère le graphe dans la div choisie 
    var chart = new google.visualization.LineChart(chartDiv);
    //Dessin du graphe
    chart.draw(dataTable, options);
    //Fonction permettant de redessiner le graphe à chaque fois que la fenêtre est redimensionnée
    //http://stackoverflow.com/questions/18979535/make-google-chart-gauge-responsive
    function resizeHandler() {
        chart.draw(dataTable, options);
    }
    if (window.addEventListener) {

        window.addEventListener('resize', resizeHandler, false);
    } else if (window.attachEvent) {

        window.attachEvent('onresize', resizeHandler);
    }
}

var start, end, values;
$(function() {
    $('#graph').on('click', function() {
        //http://stackoverflow.com/questions/11810874/how-to-get-an-input-text-value-in-javascript
        start = document.getElementById("start").value;
        end = document.getElementById("end").value;
        values = [];

        if (document.getElementById("Température").checked) {
            values.push("Température");
        }
        if (document.getElementById("Humidité").checked) {
            values.push("Humidité");
        }
        if (document.getElementById("Pression").checked) {
            values.push("Pression");
        }

        startDate = new Date(start);
        endDate = new Date(end);

        if (values.length != 0) {
            socket.emit("requestData", startDate.getTime(), endDate.getTime(), values);
        } else {

            ErrorSelect();
        }
    });
});

socket.on("returnData", function(data) {
    var unit;
    if (data.length != 0) {
        var min = FindLowest(data, values.length);
        var max = FindHighest(data, values.length);

        for (var i = 1; i < (values.length + 1); i++) {

            switch (values[i - 1]) {
                case "Température":
                    unit = "°C";
                    break;

                case "Humidité":
                    unit = "%";
                    break;

                case "Pression":
                    unit = "hPa";
                    break;
            }
            $("#min" + i.toString() + "Label").text(values[i - 1] + " minimale");
            $("#min" + i.toString() + "Label").show();
            $("#min" + i.toString()).text(min[i - 1].toString() + unit);
            $("#min" + i.toString()).show();

            $("#max" + i.toString() + "Label").text(values[i - 1] + " maximale");
            $("#max" + i.toString() + "Label").show();
            $("#max" + i.toString()).text(max[i - 1].toString() + unit);
            $("#max" + i.toString()).show();
        }

        if (values.length == 1) {
            $("#min2Label").hide();
            $("#min2").hide();
            $("#max2Label").hide();
            $("#max2").hide();
        }
        $("#chart").show();
        $("#error").hide();
        google.setOnLoadCallback(drawChart(data));
    } else {
        HideMaxMinChart()
        $("#error").text("Il n'y a pas de données enregistrées pour l'intervalle séléctionné.");
        $("#error").show();
    }
});

window.setInterval(function() {
    var endDate = new Date(end);
    var now = new Date();
    var today = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 1);
    if (today.getTime() == endDate.getTime() && values.length != 0) {
        socket.emit("requestData", startDate.getTime(), endDate.getTime(), values);
    }
}, 5000);

function HideMaxMinChart() {
    $("#min1Label").hide();
    $("#min2Label").hide();
    $("#min1").hide();
    $("#min2").hide();

    $("#max1Label").hide();
    $("#max2Label").hide();
    $("#max1").hide();
    $("#max2").hide();
    $("#chart").hide();
}

function ErrorSelect() {
    HideMaxMinChart();
    $("#error").text("Veuillez séléctionner au moins un type de donnée à afficher dans le graphique.");
    $("#error").show();
}

var options = {
    //Permet d'avoir des courbes et non des traits tout droit
    curveType: 'function',
    hAxis: {
        gridlines: {
            count: -1,
            units: {
                days: {
                    format: ['yyyy/MM/dd']
                },
                hours: {
                    format: ["MM/dd HH:00",
                        "HH:00"
                    ]
                },
                minutes: {
                    format: [
                        "HH:mm"
                    ]
                },
            }
        },
        minorGridlines: {
            units: {
                hours: {
                    format: ['hh:mm:ss']
                },
                minutes: {
                    format: ['HH:mm']
                }
            }
        }
    },
    series: {
        0: {
            targetAxisIndex: 0
        }, //Permet d'indexer chaque axe Y
        1: {
            targetAxisIndex: 1
        }
    }
};

$(function() {
    $("#error").hide();
});