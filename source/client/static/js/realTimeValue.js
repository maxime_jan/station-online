//Connection au serveur
var socket = io.connect(window.location.host);
//évènement qui récupère les données du capteur DHT
socket.on("GetDHTValue", function(data) {
    //Création de la date actuelle
    var today = new Date();
    if (data[0] != undefined && data[1] != undefined) {
        $("#currentDate").text(today.toLocaleString());
        $("#temperature").text(data[0] + "°C");
        $("#humidity").text(data[1] + "%");
    }
});

socket.on("GetBarometerValue", function(data) {
    //Création de la date actuelle
    var today = new Date();
    if (data != 0) {
        $("#currentDate").text(today.toLocaleString());
        $("#pressure").text(data + " hPa")
    }
});
